<h3 align="center">Thank you for checking out my projects!</h3> 

<p align="center">📫  Reach me at <a href="mailto:contact@jessieh.net"><strong>contact@jessieh.net</strong></a>.</p>

<p align="center">💻  Or visit my personal website <a href="https://jessieh.net"><strong>here</strong></a>.</p>
